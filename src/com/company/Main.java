package com.company;
import java.util.InputMismatchException;
import java.util.Scanner;

class ExceptionHandlingjava {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false;
        do {
            //edit here
            try {
                System.out.print("input angka pembilang = ");
                int bilang = scanner.nextInt();
                System.out.print("input angka penyebut = ");
                int sebut = scanner.nextInt();
                int hasil = pembagian(bilang, sebut);
                System.out.println("hasilnya = " + hasil);
                validInput = true;
            } catch (InputMismatchException e)
            {
                System.out.println("input harus bilangan bulat ");
                scanner.nextLine();
            } catch (ArithmeticException e)
            {
                System.out.println(e);;

            }

        } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int pembilang, int penyebut) throws Exception {
        //add exception jika penyebut bernilai 0
        if (penyebut == 0) {
            throw new ArithmeticException("Penyebut tidak bisa  bernilai 0");
        }
        return pembilang / penyebut;
    }
}


